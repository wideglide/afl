# DESCRIPTION:  Install American Fuzzy Lop
# COMMENTS:
#   This builds a container with all the AFL tools
#   necessary to get started fuzzing. This is intended
#   to be used as a base container that specific fuzzing
#   targets/sessions build from
#
# USAGE:
#	# Clone project recursively
#       git clone --recursive https://gitlab.com/wideglide/afl
#
#	# Build image
#	docker build -t afl .
#
#   # Run container per fuzzer
#	docker run --rm -it -v $(pwd):/data --name afl-session afl <afl options>
#

FROM ubuntu:16.04

# Install dependencies
RUN apt-get -qq update && apt-get -qq install -y --no-install-recommends \
      automake \
      bison \
      build-essential \
      ca-certificates \
      clang \
      gdb \ 
      libglib2.0-dev \
      libtool-bin \
      llvm-dev \
      procps \
      sudo \
      vim-tiny \
      wget \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    groupadd -g 1000 -r fuzz && \
    useradd -u 1000 --no-log-init -r -g fuzz fuzz && \
    mkdir -p /etc/sudoers.d && \
    echo "fuzz ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/fuzz && \
    chmod 0440 /etc/sudoers.d/fuzz

# Install AFL
COPY . /afl/
RUN make -C /afl && \
    make -C /afl/llvm_mode && \
    cd /afl/qemu_mode && \
    CPU_TARGET=i386 ./build_qemu_support.sh && \ 
    make -C /afl install && \
    cd / && rm -rf /afl


USER fuzz

COPY tools/set_umask.sh set_umask.sh
WORKDIR /data
ENTRYPOINT ["afl-fuzz"]
