#!/bin/bash

# variable defaults
AFL=afl-fuzz
QM=
IN=inputs
OUT=outputs
TARGET=
PRE=F
N=2
DRY=false

usage()
{
    echo "Usage: ./start_fuzzing.sh [-a afl-fuzz] [-Q] [-i dir] [-o dir] [-n #] [-p SESSION] -t \"target_command\" [-D]"
}

while [ "$1" != "" ]; do
    case $1 in 
        -a | --afl_path )
            shift
            AFL=$1
            ;;
        -Q | --afl_path )
            QM="-Q"
            ;;
        -i | --input )
            shift
            IN=$1
            ;;
        -o | --output )
            shift
            OUT=$1
            ;;
        -p | --prefix )
            shift
            PRE=$1
            ;;
        -n | --instances )
            shift
            N=$1
            ;;
        -t | --target )
            shift
            TARGET=$1
            ;;
        -D | --dry_run )
            DRY=true
            ;;
        -h | --help )
            usage
            exit 1
            ;;
        * )
            usage
            exit 1
    esac
    shift
done

set $TARGET
if [ -x $1 ]; then
	echo "[+] running $1 with afl-showmap"
	afl-showmap $QM -o /dev/null -- $1 < <(echo seed)
else
	echo "[-] ERROR: binary ( $1 ) does not exist or not executable."
fi

read -p "Continue with fuzz? "

echo "nohup $AFL $QM -m 4096 -i $IN -o $OUT -M ${PRE}00 -- $TARGET >> ${PRE}00.log &"

if [ "$DRY" = false ]; then
	mkdir -p /tmp/afl_sessions
	echo $$ > "/tmp/afl_sessions/afl_${PRE}_$(date +%y%m%d_%H%M%S).PGID"
	nohup $AFL $QM -m 4096 -i $IN -o $OUT -M "${PRE}00" -- $TARGET >> "${PRE}00.log" &
fi

N=$[$N-1]
for i in $(seq -w 01 $N); do
	echo "nohup $AFL $QM -m 4096 -i $IN -o $OUT -S ${PRE}${i} -- $TARGET >> ${PRE}${i}.log &"
	if [ "$DRY" = false ]; then
		sleep 4
		nohup $AFL $QM -m 4096 -i $IN -o $OUT -S "${PRE}${i}" -- $TARGET >> "${PRE}${i}.log" &
        fi
done

