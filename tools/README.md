

# A collection of useful tools for fuzzing with AFL
- [afl-cov](https://github.com/mrash/afl-cov)
- [afl-monitor](https://github.com/reflare/afl-monitor)
- [afl-trivia](https://github.com/bnagy/afl-trivia)
    - `afl-pause` & `afl-resume` Pause and resume a session of fuzzers 
    - `afl-stop`  Stop a session of fuzzers
- [afl-utils](https://gitlab.com/rc0r/afl-utils)
- [afl-patches](https://github.com/vanhauser-thc/afl-patches)

