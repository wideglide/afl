

# AFL 
AFL [README](docs/README.txt)

AFL [quick start guide](docs/QuickStartGuide.txt)

### CI generated artifacts
- AFL for Ubuntu 14.04: https://gitlab.com/wideglide/afl/-/jobs/artifacts/master/download?job=build:trusy
- AFL for Ubuntu 16.04: https://gitlab.com/wideglide/afl/-/jobs/artifacts/master/download?job=build:xenial
- AFL for Ubuntu 18.04: https://gitlab.com/wideglide/afl/-/jobs/artifacts/master/download?job=build:bionic

- Download and install
```shell
wget -qO /tmp/afl.zip https://gitlab.com/wideglide/afl/-/jobs/artifacts/master/download?job=build:xenial
sudo unzip /tmp/afl.zip -d /
```

### Docker Container Registry
- registry.gitlab.com/wideglide/afl/i386:16.04


## Changes
- add __-L__ command line arg to specify a time limit. {suffix of s,m,h,d supported}   
  `afl-fuzz -i in -o out -L 3d -- ./bin/file` to fuzz for three days
